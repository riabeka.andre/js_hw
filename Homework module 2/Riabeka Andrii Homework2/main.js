class Hamburger {
    constructor (size, stuffing, toppings) {
        try {
            if (arguments.length < 2) {
                throw  new HamburgerException("Two options needed, given: " + arguments.length);
            }
            if (allowedSize.indexOf(size) < 0) {
                throw  new HamburgerException('Error! Invalid size');
            }
            if (allowedStuffing.indexOf(stuffing) < 0) {
                throw  new HamburgerException('Error! Invalid stuffing');
            }
        }
        catch (error) {
            console.log(error);
        }
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    }
    getSize () {
        return this.size;
    }
    getStuffing () {
        return this.stuffing;
    }
    getToppings () {
        return this.toppings;
    }
    addTopping (topping) {
        try {
            if (allowedToppings.indexOf(topping) < 0) {
                throw new HamburgerException("Incorrect topping");
            }
            if (arguments.length >= 1) {
                throw new HamburgerException("So much toppings, do you really want extra calories?");
            }
        }
        catch (error) {
            console.log(error);
        }
        this.toppings.push(topping);
    }
    removeTopping(topping) {
        try {
            if (allowedToppings.indexOf(topping) < 0) {
                throw new HamburgerException("Incorrect topping");
            }
            if (this.toppings.indexOf(topping) < 0) {
                throw new HamburgerException("None topping");
            }
        }
        catch (error) {
            console.log(error);
        }
        this.toppings.pop(topping);
    }
    calculatePrice () {
        let price = this.size.price;
        let stuffing = this.stuffing.price;
        price += stuffing;
        let toppings = this.getToppings();
        toppings.forEach(function (item) {
            price += item['price'];
        });
        return price;
    }
    calculateCalories () {
        let calories = this.size.calories;
        let stuffing = this.stuffing.calories;
        calories += stuffing;
        let toppings = this.getToppings();
        toppings.forEach(function (item) {
            calories += item['calories'];
        });
        return calories;
    }
    get hamburgerName() {
        if (this.topping === null) {
            return `${this.size.name} with ${this.stuffing.name}`;
        }
        if (this.toppings.length === 1) {
            return `${this.size.name} with ${this.stuffing.name} and ${this.toppings[0].name}`
        }
        if (this.toppings.length === 2) {
            return `${this.size.name} with ${this.stuffing.name} and ${this.toppings[0].name} and ${this.toppings[1].name}`
        } else {
            return `${this.size.name} with ${this.stuffing.name}`;
        }
    }
    set hamburgerName(value) {
        [this.size.name, this.stuffing.name] = value.split(" ")
    }
}
function HamburgerException (message) {
    this.message = message;
}
let SIZE_SMALL = {
    name:'Small',
    price: 50,
    calories: 20
};
let SIZE_LARGE = {
    name:'Large',
    price: 100,
    calories: 40
};
let STUFFING_CHEESE = {
    name: 'Cheese',
    price: 10,
    calories: 20
};
let STUFFING_SALADE = {
    name: 'Salade',
    price: 20,
    calories: 5
};
let STUFFING_POTATO = {
    name: 'Potato',
    price: 15,
    calories: 10
};
let TOPPING_MAYO = {
    name: 'Mayonaise',
    price: 20,
    calories: 5
};
let TOPPING_SPICE = {
    name: 'Spices',
    price: 15,
    calories: 0
};
let allowedSize = [SIZE_SMALL, SIZE_LARGE];
let allowedStuffing = [STUFFING_CHEESE, STUFFING_POTATO, STUFFING_SALADE];
let allowedToppings = [TOPPING_MAYO, TOPPING_SPICE];
const hamburger = new Hamburger(SIZE_SMALL, STUFFING_CHEESE);
hamburger.addTopping(TOPPING_SPICE);
hamburger.addTopping(TOPPING_MAYO);
console.log(hamburger);
console.log('Full name:', hamburger.hamburgerName);
console.log("Price: %f ", hamburger.calculatePrice(), 'UAH');
console.log("Calories: %f ", hamburger.calculateCalories());
console.log("Price with sauce: %f " , hamburger.calculatePrice(), 'UAH');
console.log("Is hamburger large: %s " , hamburger.getSize() === SIZE_LARGE);
console.log("Have %d toppings ", hamburger.getToppings().length);