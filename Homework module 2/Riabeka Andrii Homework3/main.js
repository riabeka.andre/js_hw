let celulles = document.getElementsByTagName("td");
let red = document.querySelectorAll('.red');
let green = document.querySelectorAll('.green');
let blue = document.querySelectorAll('.blue');
let score = document.getElementById('score');
let score2 = document.getElementById('score2');
let winner = document.getElementById('winner');
let levels = document.getElementById('levels');
let norm = document.getElementById('norm');
let easy = document.getElementById('easy');
let hard = document.getElementById('hard');
let b, c, d;

class Essey {
    constructor(){
    }
    aNumber () {
        let i = Math.floor(Math.random() * 100);
        if(celulles[i].classList.contains('green') || celulles[i].classList.contains('red')){
            i ++;
        }
        if(i >= celulles.length) {
            i= 0;
        }
        celulles[i].classList.add('blue');
        celulles[i].onclick = function () {
            if(celulles[i].classList.contains('blue')) {
                celulles[i].classList.remove('blue');
                celulles[i].classList.add('green');
            }
        };
    }
    getRed() {
        for (let i = 0; i < celulles.length; i++) {
            if (celulles[i].classList.contains('blue')) {
                celulles[i].classList.remove('blue');
                celulles[i].classList.add('red');
            }
        }
    }
    showWinner(){
        let arr = [];
        let arr2 = [];
        for(let i = 0; i < celulles.length; i++) {
            if (celulles[i].classList.contains('green')) {
                arr.push(celulles[i]);
                score.innerText = "gamer's score is " + arr.length;
            }
        }
        for(let i = 0; i < celulles.length; i++) {
            if (celulles[i].classList.contains('red')) {
                arr2.push(celulles[i]);
                score2.innerText = "computor's score is " + arr2.length;
            }
        }
        if(arr2.length >= 50) {
            winner.innerText = 'Winner is computer';
            clearInterval(b);
            clearInterval(c);
            levels.classList.remove('hidden');
        }
        if(arr.length >= 50) {
            winner.innerText = 'Winner is gamer';
            clearInterval(b);
            clearInterval(c);
            levels.classList.remove('hidden');
        }
    }
    makeIntervalAnumber(x) {
        b = setInterval(this.aNumber, x);
    }
    makeIntervalGetRed(z) {
        c = setInterval(this.getRed, z);
    }
    makeIntervalShowWinner(n) {
        d = setInterval(this.showWinner, n)
    }
    start (delay) {
        for(let i = 0; i < celulles.length; i++) {
            celulles[i].classList.remove('green')
            celulles[i].classList.remove('blue')
            celulles[i].classList.remove('red')
        }

        this.showWinner.arr = [];
        this.showWinner.arr2 = [];
        score2.innerText = "computor's score is ";
        score.innerText = "gamer's score is ";

        this.makeIntervalAnumber(delay + 1);
        this.makeIntervalGetRed(delay);
        this.makeIntervalShowWinner(100);
        levels.classList.add('hidden');
    }
}
let essey = new Essey();
easy.onclick = function () {
    essey.start(1500)
};
norm.onclick = function () {
    essey.start(1000)
};
hard.onclick = function () {
    essey.start(500)
};