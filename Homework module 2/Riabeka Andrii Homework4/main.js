
class Cards {
    constructor() {
    }
    newElement () {
        const divFirst = document.createElement('div');
        divFirst.setAttribute('class', 'addColumn');
        divFirst.setAttribute('id', 'idAddColumn');
        document.body.appendChild(divFirst);
        const divTxt = document.createElement('span');
        divTxt.setAttribute('class', 'addColumnText');
        divTxt.setAttribute('id', 'idAddColumnText');
        divTxt.textContent = '+ Добавте ещe одну колонку';
        divFirst.append(divTxt);
        divTxt.addEventListener('click', () => this.newElementExpanded());
    }
    getElement () {
        document.addEventListener('load', this.newElement());
    }
    newElementExpanded () {
        const div = document.getElementById('idAddColumn');
        const text = document.getElementById('idAddColumnText');
        const inputFirst = document.createElement('input');
        inputFirst.setAttribute('class', 'addColumnInput');
        inputFirst.setAttribute('id', 'idAddColumnInput');
        inputFirst.setAttribute('type', 'text');
        inputFirst.setAttribute('value', 'Ведіть заголовок переліку');
        inputFirst.addEventListener('click', function () {
            inputFirst.setAttribute('value', '');
        });
        div.prepend(inputFirst);
        const sbmButton = document.createElement('button');
        sbmButton.setAttribute('class', 'addColumnButton');
        sbmButton.setAttribute('id', 'idAddColumnButton');
        sbmButton.textContent = '+Добавити перелік';
        div.append(sbmButton);
        text.textContent = '';
        sbmButton.addEventListener('click', () => this.createCard());
    }
    createCard() {
        const valueChaced = document.getElementById('idAddColumnInput').value;
        if(valueChaced === '' || valueChaced === 'Ведіть заголовок переліку' ) return;
        const container = document.createElement('div');
        container.setAttribute('class', 'mainDiv');
        container.setAttribute('id', 'idMainDiv');
        document.body.appendChild(container);
        const txtCont = document.querySelector('.addColumnInput').value;
        const headers = document.createElement('div');
        headers.setAttribute('class', 'mainHeaders');
        headers.textContent = txtCont;
        container.append(headers);
        const noteContainer = document.createElement('div');
        noteContainer.setAttribute('class', 'divContainer');
        noteContainer.setAttribute('id', 'list');
        container.append(noteContainer);
        noteContainer.addEventListener('dragover', (e) => this.dragOverTarget (e, noteContainer));
        noteContainer.addEventListener('drop', (e) => this.dropTarget(e, noteContainer));
        const sortT =  document.createElement('div');
        sortT.setAttribute('class', 'sortHeaders');
        sortT.textContent = '...';
        container.append(sortT);
        sortT.addEventListener('click', () => this.sortButton(noteContainer));
        const inputs = document.createElement('input');
        inputs.setAttribute('class', 'inputInside');
        inputs.setAttribute('id', 'addInput');
        inputs.setAttribute('type', 'text');
        inputs.setAttribute('value', 'Введіть заголовок для цієї картки');
        container.append(inputs);
        inputs.addEventListener('click', function () {
            inputs.setAttribute('value', '');
        });
        const button = document.createElement('div');
        button.setAttribute('class', 'buttonInside');
        button.setAttribute('id', 'idButtonInside');
        button.textContent = '+ Добавити картку';
        container.append(button);
        button.addEventListener('click', () => this.createNotes(inputs, noteContainer));
    }
    createNotes(inputs, noteContainer) {
        let divCover = document.createElement('div');
        divCover.setAttribute('class', 'itemText');
        divCover.setAttribute('id', 'idDD');
        divCover.setAttribute('draggable', 'true');
        divCover.textContent = inputs.value;
        noteContainer.append(divCover);
        divCover.addEventListener('dragstart', (e) => this.dragStTarget(e, divCover));
    }
    sortButton (noteContainer) {
        let toSort = noteContainer.children;
        let newList = [];

        for (let z = 0; z < toSort.length; z++) {
            newList.push(toSort[z].textContent);
        }

        let results = newList.sort(function compare(a, b) {
            if (a < b) return -1;
            if (a > b) return 1;
            if (a === b) return 0;
            return newList;
        });

        noteContainer.innerHTML = "";
        for (let i = 0; i < results.length; i++) {
            let divCover = document.createElement('div');
            divCover.setAttribute('class', 'itemText');
            divCover.setAttribute('id', 'idDD');
            divCover.setAttribute('draggable', 'true');
            divCover.textContent = results[i];
            noteContainer.append(divCover);
            divCover.addEventListener('dragstart', (e) => this.dragStTarget(e, divCover));
        }
    }
    dragOverTarget (event, noteContainer) {
        noteContainer.style.backgroundColor = 'orange';
        event.preventDefault();
    }
    dropTarget (event, noteContainer) {
        noteContainer.style.backgroundColor = 'red';
        event.preventDefault();
        let target = event.target;
        let srcId = event.dataTransfer.getData('srcId');
        if (target.classList.contains('divContainer')) {
            target.append(document.getElementById(srcId));
        }
    }
    dragStTarget(event, divCover) {
        divCover.style.backgroundColor = 'blue';
        event.dataTransfer.setData('srcId', event.target.id);
    }
}
let theCard = new Cards();
theCard.getElement();