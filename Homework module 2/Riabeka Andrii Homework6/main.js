const details = document.getElementById('details');
async function callAPI() {
    const res = await axios.get(`https://api.ipify.org/?format=json`
    ).then(response => response.data.ip);
    let ip = res;

    async function callID() {
        const result = await axios.get(`http://ip-api.com/json/${ip}?lang=ru&fields=continent,country,region,city,district`
        ).then(response => response.data);
        const list = document.createElement('li');
        const ul = document.createElement('ul');
        details.append(ul);
        ul.append(list);
        list.innerHTML = 'Континент: ' + `${result.continent}` + '</br>' +
            'Країна: ' + `${result.country}` + '</br>' +
            'Регіон: ' + `${result.region}` + '</br>' +
            'Місто: ' + `${result.city}` + '</br>' +
            'Район в місті: ' + `${result.district}`;
    }
    callID();
}
