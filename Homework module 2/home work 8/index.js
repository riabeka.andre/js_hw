const details = document.getElementById('details');
let addPost = document.getElementById('addPost');
let form = document.getElementById('divForm');
let publish = document.getElementById('finalAdd');
let titleValue = document.getElementById('title');
let postValue = document.getElementById('post');

async function callAPI() {
    const res = await axios.get(`https://jsonplaceholder.typicode.com/users`
    ).then(response => response.data);
    // let data = res;
    for (let i = 0; i < res.length; i++){

        const result = await axios.get(`https://jsonplaceholder.typicode.com/posts`
        ).then(response => response.data);
        for(let j = 0; j < result.length; j++){
            if(result[j].userId === res[i].id){
                const list = document.createElement('li');
                const ul = document.createElement('ul');
                const div = document.createElement('div');
                div.setAttribute('class', 'div');
                details.append(div);
                div.append(ul);
                ul.append(list);
                list.innerHTML = `${res[i].name} ${res[i].email} ${result[j].id}`;
                const list2 = document.createElement('li');
                const list3 = document.createElement('li');
                ul.append(list2);
                ul.append(list3);
                list2.innerHTML = result[j].title;
                list3.innerHTML = result[j].body;
                let x  = document.createElement('div');
                x.setAttribute('class', 'del');
                div.append(x);
                x.innerHTML = 'Delete';
                let modify = document.createElement('button');
                modify.setAttribute('class', 'modify');
                div.append(modify);
                modify.innerHTML = 'Change post';
                let change = document.querySelectorAll(".modify");
                let del = document.querySelectorAll(".del");
                let divv = document.querySelectorAll(".div");
                change[j].onclick = function modifyPost(){
                    let inputTitle = document.createElement('input');
                    inputTitle.style.width = '500px';
                    inputTitle.style.height = '20px';
                    divv[j].append(inputTitle);
                    inputTitle.value = list2.textContent;
                    let inputBody = document.createElement('TEXTAREA');
                    // inputBody.setAttribute('type', 'textarea')
                    inputBody.style.width = '500px';
                    inputBody.style.height = '70px';
                    divv[j].append(inputBody);
                    inputBody.value = list3.textContent;
                    list2.classList.add('hidden');
                    list3.classList.add('hidden');
                    change[j].classList.add('hidden');
                    const save = document.createElement('button');
                    div.append(save);
                    save.innerHTML = 'save';
                    save.onclick = function () {
                        list2.innerHTML = inputTitle.value;
                        list3.innerHTML = inputBody.value;
                        list2.classList.remove('hidden');
                        list3.classList.remove('hidden');
                        change[j].classList.remove('hidden');
                        inputTitle.classList.add('hidden');
                        inputBody.classList.add('hidden');
                        save.classList.add('hidden');
                        fetch(`https://jsonplaceholder.typicode.com/posts/${result[j].id}`, {
                            method: 'PUT',
                            body: JSON.stringify({
                                id: result[j].id,
                                title: inputTitle.value,
                                body: inputBody.value,
                                userId: result[j].userId
                            }),
                            headers: {
                                "Content-type": "application/json; charset=UTF-8"
                            }
                        })
                            .then(response => response.json())
                            .then(json => console.log(json))
                    }
                };
                del[j].onclick = async function () {
                    divv[j].classList.add('hidden');
                    const result2 = await axios.delete(`https://jsonplaceholder.typicode.com/posts/${result[j].id}`
                    ).then(response => response.data);
                    console.log(result2)
                };
                addPost.onclick = function () {
                    form.classList.remove('hidden');
                    form.classList.add('formActive');
                    addPost.classList.add('hidden');
                };
                publish.onclick = async function (event) {
                    event.preventDefault();
                    if(titleValue.value !== '' && postValue.value !== '') {

                        const list = document.createElement('li');
                        const ul = document.createElement('ul');
                        const div = document.createElement('div');
                        div.setAttribute('class', 'div');
                        details.append(div);
                        div.append(ul);
                        ul.append(list);
                        list.innerHTML = `${res[0].name} ${res[0].email}`;
                        const list2 = document.createElement('li');
                        const list3 = document.createElement('li');
                        ul.append(list2);
                        ul.append(list3);
                        list2.innerHTML = titleValue.value;
                        list3.innerHTML = postValue.value;
                        form.classList.add('hidden');
                        addPost.classList.remove('hidden');
                        fetch(`https://jsonplaceholder.typicode.com/posts`, {
                                method: 'POST',
                                body: JSON.stringify({
                                    title: `${titleValue.value}`,
                                    body: `${postValue.value}`,
                                    userId: 1
                                }),
                                headers: {
                                    "Content-type": "application/json; charset=UTF-8"
                                }
                            }
                        ).then(response => response.json())
                            .then(json => console.log(json));

                    let x  = document.createElement('div');
                    x.setAttribute('class', 'del');
                    div.append(x);
                    x.innerHTML = 'Delete';
                    let modify = document.createElement('button');
                    modify.setAttribute('class', 'modify');
                    div.append(modify);
                    modify.innerHTML = 'Change post';
                }
                    console.log(modify);
                        modify.onclick = function (){
                            console.log('ok');
                            let inputTitle = document.createElement('input');
                            inputTitle.style.width = '500px';
                            inputTitle.style.height = '20px';
                            div.append(inputTitle);
                            inputTitle.value = list2.textContent;
                            let inputBody = document.createElement('TEXTAREA');
                            // inputBody.setAttribute('type', 'textarea')
                            inputBody.style.width = '500px';
                            inputBody.style.height = '70px';
                            div.append(inputBody);
                            inputBody.value = list3.textContent;
                            list2.classList.add('hidden');
                            list3.classList.add('hidden');
                            modify.classList.add('hidden');
                            const save = document.createElement('button');
                            div.append(save);
                            save.innerHTML = 'save';
                            save.onclick = function () {
                                list2.innerHTML = inputTitle.value;
                                list3.innerHTML = inputBody.value;
                                list2.classList.remove('hidden');
                                list3.classList.remove('hidden');
                                modify.classList.remove('hidden');
                                inputTitle.classList.add('hidden');
                                inputBody.classList.add('hidden');
                                save.classList.add('hidden');
                                fetch(`https://jsonplaceholder.typicode.com/posts/${result[j].id+1}`, {
                                    method: 'PUT',
                                    body: JSON.stringify({
                                        id: 101,
                                        title: inputTitle.value,
                                        body: inputBody.value,
                                        userId: 1
                                    }),
                                    headers: {
                                        "Content-type": "application/json; charset=UTF-8"
                                    }
                                })
                                    .then(response => response.json())
                                    .then(json => console.log(json))
                            }
                        };
                    x.onclick = async function () {
                        div.classList.add('hidden');
                        const result2 = await axios.delete(`https://jsonplaceholder.typicode.com/posts/${result[j].id}`
                        ).then(response => response.data);
                        console.log(result2)
                    };

                }
            }
        }
    }

}
callAPI();

