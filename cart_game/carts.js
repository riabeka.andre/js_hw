let battleFieldArr = [];
let cartsOfOneSuit = [];
let usedCartsRangs = [];



let carts = [{
        suit: 'heart',
        rang: 2,
        src: 'carts/heart/twoHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 3,
        src: 'carts/heart/threeHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 4,
        src: 'carts/heart/fourHert.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 5,
        src: 'carts/heart/fiveHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 6,
        src: 'carts/heart/sixheart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 7,
        src: 'carts/heart/sevenHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 8,
        src: 'carts/heart/eightHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 9,
        src: 'carts/heart/nineHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 10,
        src: 'carts/heart/tenHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 11,
        src: 'carts/heart/jackHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 12,
        src: 'carts/heart/queenHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 13,
        src: 'carts/heart/kingHeart.png',
        back: 'carts/back.png'
    },
    {
        suit: 'heart',
        rang: 14,
        src: 'carts/heart/tuse.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 2,
        src: 'carts/spade/twoSpade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang:3,
        src: 'carts/spade/threeSpade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 4,
        src: 'carts/spade/four-spade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 5,
        src: 'carts/spade/five-spad.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 6,
        src: 'carts/spade/six-spade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 7,
        src: 'carts/spade/seven-apde.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 8,
        src: 'carts/spade/eight-spade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 9,
        src: 'carts/spade/nine-spade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 10,
        src: 'carts/spade/ten-spade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 11,
        src: 'carts/spade/jack-spade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 12,
        src: 'carts/spade/qeen-spade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 13,
        src: 'carts/spade/king-spade.png',
        back: 'carts/back.png'
    },
    {
        suit: 'spade',
        rang: 14,
        src: 'carts/spade/holeinoneSpade.png',
        back: 'carts/back.png'
    },
]