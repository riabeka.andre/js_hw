import React, {Component} from 'react';
import PartOne from './PartOne'
import PartTwo from './PartTwo'

import '../App.css';
class App extends Component{
    constructor(props){
        super(props);
    }
    render() {
        return(
            <div className='buttons'>
            <PartOne />
            <PartTwo/>
            </div>

    )
    }

}

export default App;