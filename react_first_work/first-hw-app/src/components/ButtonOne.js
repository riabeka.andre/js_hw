import React, {Component} from "react";
import App from "./App";
class ButtonOne extends Component{
    constructor(props){
        super(props)
    }
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <button className='buttonPrincipal' onClick={this.props.showModalOne}>Open first modal</button>
                </header>
            </div>
        );
    }

}
export default ButtonOne;