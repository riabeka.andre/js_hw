import React, {Component} from "react";
import App from "./App";
class ButtonTwo extends Component{
    constructor(props){
        super(props)
    }
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <button className='buttonPrincipal' onClick={this.props.showModalTwo}>Open second modal</button>
                </header>
            </div>
        );
    }

}
export default ButtonTwo;