import React, {Component} from "react";
import App from "./App";
import PropTypes from "prop-types";
class ModalOne extends Component {
    constructor(props){
        super(props)
    }
    render() {
        ModalOne.propTypes = {
            sayYes: PropTypes.func,
            sayCancel: PropTypes.func,
            closeButton: PropTypes.bool
        };
        const {modalOne} = this.props;
        const body = <section className='section'>{modalOne.text}</section>;
        const header = <h2 className='header'>{modalOne.header}{this.props.closeButton}<div className='closeButton'>{modalOne.closeButton}</div> </h2>
        return (
            <>
                <div className='shadowForModal' onClick={this.props.sayCancel}>

                </div>
                <div className="ModalOne">
                    {header}
                    {body}
                    <button className='button' onClick={this.props.sayYes}> {modalOne.buttonOne}</button>
                    <button className='button' onClick={this.props.sayCancel}>{modalOne.buttonTwo}</button>
                </div>
            </>


        );
    }
}


export default ModalOne;