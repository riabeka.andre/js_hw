import React, {Component} from "react";
import PropTypes from 'prop-types';

class ModalTwo extends Component {
    constructor(props){
        super(props)
    }
    render() {
        ModalTwo.propTypes = {
            sayYes: PropTypes.func,
            sayCancel: PropTypes.func,
            closeButton: PropTypes.bool
        };
        const {modalTwo} = this.props;
        const body = <section className='section'>{modalTwo.text}</section>;
        const header = <h2 className='headerTwo'>{modalTwo.header}{this.props.closeButton}<div className='closeButton'>{modalTwo.closeButton}</div> </h2>;
        console.log(this.props)
        return (
            <>
                <div className='shadowForModal' onClick={this.props.sayCancel}>
                </div>
                <div className="ModalTwo">
                    {header}
                    {body}
                    <button className='buttonTwo' onClick={this.props.sayYes}> {modalTwo.buttonOne}</button>
                    <button className='buttonTwo' onClick={this.props.sayCancel}>{modalTwo.buttonTwo}</button>
                </div>
            </>
        );

    }

}


export default ModalTwo;