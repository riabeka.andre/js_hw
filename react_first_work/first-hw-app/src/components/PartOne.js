import React, {Component} from 'react';
import ButtonOne from "./ButtonOne";
import ModalOne from "./ModalOne";
import properties from "./properties";
class PartOne extends Component{
    constructor(props){
        super(props);
        this.state = {
            isOpen: false
        }
    }
    modalFirst = () => {
        this.setState({
            isOpen: true

        })
    };
    toAccept = () => {
        this.setState({
            isOpen: false
        });
        console.log('accepted');
    };
    toDenie = () => {
        this.setState({
            isOpen: false
        });
        console.log('Denied');
    };
    closeButton = true;
    render() {
        const body = this.state.isOpen && <ModalOne modalOne={properties[0]}
                                                    sayYes={this.toAccept}
                                                    sayCancel={this.toDenie}/>;
        const button =  !this.state.isOpen && < ButtonOne showModalOne={this.modalFirst}/>;
        return(
            <>
                {button}
                {body}
            </>

        )
    }

}
export default PartOne;