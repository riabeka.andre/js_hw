import React, {Component} from 'react';
import ButtonTwo from "./ButtonTwo";
import ModalTwo from "./ModalTwo";
import properties from "./properties";

class PartTwo extends Component{
    constructor(props){
        super(props)
        this.state = {
            isOpen: false,
        }

    }
    modalSecond = () => {
        this.setState({
            isOpen: true

        })
    };
    toAccept = () => {
        this.setState({
            isOpen: false
        });
        console.log('accepted');
    };
    toDenie = () => {
        this.setState({
            isOpen: false,

        });
        console.log('Denied');
    };
    closeButton = true;

    render() {
        const body = this.state.isOpen && <ModalTwo modalTwo={properties[1]}
                                                    sayYes={this.toAccept}
                                                    sayCancel={this.toDenie}
                                                    closeButton={this.closeButton}/>;
        const button =  !this.state.isOpen && < ButtonTwo  showModalTwo={this.modalSecond}/>
        return (
            <>
                {button}
                {body}
            </>

        )
    }
}
export default PartTwo;