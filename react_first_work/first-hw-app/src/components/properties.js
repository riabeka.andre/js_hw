export default [
    {
        id: 1,
        header: 'Do you want to delete this file?',
        closeButton: 'X',
        text: 'Once you delete this file, it won’t be possible to undo this action. \n' +
            'Are you sure you want to delete it?',
        buttonOne: 'OK',
        buttonTwo: 'Cancel'
    },
    {
        id: 2,
        header: 'Do you want to delete this file?',
        closeButton: 'X',
        text: 'Once you delete this file, it won’t be possible to undo this action. \n' +
            'Are you sure you want to delete it?',
        buttonOne: 'OK',
        buttonTwo: 'Cancel'
    }
];
