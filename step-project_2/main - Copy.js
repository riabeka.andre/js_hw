
let createdCards = [];
let params = {
    doctor: '',
    id: ''
};
let title = document.querySelector('.title');
const url = 'http://cards.danit.com.ua/cards';
const data = {
    email: 'nataliia.ivaniv@gmail.com',
    password: 'ivaniv30',
};
const config = {headers: {
        Authorization: 'Bearer 63e09095c5e8'
    }};

class Visit {
    constructor(){

    }
    createVisit() {
        const button = document.querySelector(".button");
        button.addEventListener("click", () => this.defaultOption());
        const cross = document.querySelector('.close');
        cross.addEventListener('click', function () {
            const visitCard = document.querySelector('.modal');
            visitCard.classList.remove('active');
            const option = document.getElementById('idVisitOption');
            option.value = "";
        });
        const createBtn = document.querySelector('.create');
        createBtn.addEventListener('click', () => this.createClient());
    };

    defaultOption() {
        const visitCard = document.querySelector('.modal');
        visitCard.classList.add('active');
        const option = document.querySelectorAll('.docOption');

        const inputs = document.querySelectorAll('.name');
        const lables = document.querySelectorAll('.label');
        if (option[0]) {
            for (let i = 0; i < inputs.length; i++) {
                inputs[i].classList.remove('active');
            }
            for (let i = 0; i < lables.length; i++) {
                lables[i].classList.remove('active');
            }
        }
    }

    selectOption() {
        const option = document.getElementById('idVisitOption');
        const inputs = document.querySelectorAll('.name');
        const lables = document.querySelectorAll('.label');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('click', function () {
                inputs[i].value = '';
            });
        }

        option.addEventListener('change', function () {

            if (option.value === 'Cardiologist') {
                for (let i = 0; i < inputs.length; i++) {
                    inputs[i].classList.add('active');
                    inputs[3].classList.remove('active');
                    inputs[i].value = '';
                }
                for (let i = 0; i < lables.length; i++) {
                    lables[i].classList.add('active');
                    lables[3].classList.remove('active');
                }
            }
            if (option.value === "") {
                for (let i = 0; i < inputs.length; i++) {
                    inputs[i].classList.remove('active');
                    inputs[i].value = '';
                }
                for (let i = 0; i < lables.length; i++) {
                    lables[i].classList.remove('active');
                }
            }
            if (option.value === 'Dentist') {
                for (let i = 0; i < inputs.length; i++) {
                    inputs[i].classList.remove('active');
                    inputs[0].classList.add('active');
                    inputs[2].classList.add('active');
                    inputs[3].classList.add('active');
                    inputs[7].classList.add('active');
                    inputs[i].value = '';
                }
                for (let i = 0; i < lables.length; i++) {
                    lables[i].classList.remove('active');
                    lables[0].classList.add('active');
                    lables[2].classList.add('active');
                    lables[3].classList.add('active');
                    lables[7].classList.add('active');
                }
            }
            if (option.value === 'Therapist') {
                for (let i = 0; i < inputs.length; i++) {
                    inputs[i].classList.remove('active');
                    inputs[0].classList.add('active');
                    inputs[1].classList.add('active');
                    inputs[2].classList.add('active');
                    inputs[7].classList.add('active');
                    inputs[i].value = '';
                }
                for (let i = 0; i < lables.length; i++) {
                    lables[i].classList.remove('active');
                    lables[0].classList.add('active');
                    lables[1].classList.add('active');
                    lables[2].classList.add('active');
                    lables[7].classList.add('active');
                }
            }
            params.doctor = option.value;
        });
    }

    createClient() {

        const card = document.createElement('div');
        card.setAttribute('class', 'card');
        card.setAttribute('draggable', 'true');
        card.style.zIndex = 2;
        const cross = document.createElement('div');
        cross.setAttribute('class', 'cross');
        cross.innerText = 'X';
        card.append(cross);
        const lables = document.querySelectorAll('.label.active');
        const visitCard = document.querySelector('.modal');
        const option = document.getElementById('idVisitOption');
        const inputs = document.querySelectorAll('.name.active');
        const wrapperCard = document.querySelector('.wrapperCard');
        wrapperCard.append(card);
        cross.addEventListener('click', function () {
            card.classList.remove('active');
            createdCards.pop(card);
            axios.delete(`http://cards.danit.com.ua/cards/${params.id}`, config
            ).then(response => {console.log(response)
            });
            if(createdCards.length > 0) {
                title.classList.add('active')
            }
            if(createdCards.length == 0){
                title.classList.remove('active')
            }
            console.log(createdCards.length);

        });
        const wraperDrag = document.createElement('div');
        wraperDrag.setAttribute('draggable', 'true');
        wraperDrag.setAttribute('class', 'wraperDrag');
        card.append(wraperDrag);
        const fio = document.createElement('div');
        fio.setAttribute('class', 'infoInCards');
        fio.setAttribute('id', 'fio');
        wraperDrag.append(fio);
        const doctor = document.createElement('div');
        doctor.setAttribute('class', 'infoInCards');
        doctor.setAttribute('id', 'doctor');
        wraperDrag.append(doctor);
        const otherInf = document.createElement('div');
        otherInf.setAttribute('class', 'otherInf');
        otherInf.setAttribute('id', 'otherInf');
        card.append(otherInf);
        const addInformation = document.createElement('div');
        addInformation.setAttribute('class', 'addInformation');
        addInformation.setAttribute('id', 'addInformation');
        addInformation.textContent = 'Show more information';
        card.append(addInformation);
        addInformation.addEventListener('click', function () {
            card.classList.add('active-test');
            otherInf.classList.remove('hidden');
            addInformation.classList.add('active');
            let hide = document.createElement('div');
            card.append(hide);
            hide.innerHTML = 'Hide information';
            hide.onclick = function(){
                card.classList.remove('active-test');
                otherInf.classList.add('hidden');
                hide.classList.add('hidden');
                addInformation.classList.remove('active');
            }
        });

        let arr = [];
        let arrOther = [];
        let arrSecond = [];
        let arrOtherSecond = [];

        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].value !== '') {
                card.classList.add('active');
                arr.push(inputs[i].value);
                fio.innerText = arr[0];
                doctor.innerText = option.value;
                arrOther = arr.slice(1);

            }

        }
        for (let i = 0; i < inputs.length - 1; i++) {
            if (inputs[i].value == '')
                card.classList.remove('active');
        }
        for (let i = 0; i < lables.length; i++) {
            arrSecond.push(lables[i].textContent);
            arrOtherSecond = arrSecond.slice(1);

        }
        for (let i = 0; i < inputs.length ; i++) {
            params[arrSecond[i]] = arr[i];

        }
        for (let i = 0; i < arrOther.length; i++) {

            const list = document.createElement('li');
            const ul = document.createElement('ul');
            otherInf.append(ul);
            ul.append(list);
            list.innerHTML = `${arrOtherSecond[i]} : ${arrOther[i]}`;
            otherInf.classList.add('hidden');

        }



        visitCard.classList.remove('active');
        option.value = "";
        createdCards.push(card);
        axios.post('http://cards.danit.com.ua/login', data, config
            ).then(response => {});

        async function f() {
            const res = await axios.post(url, params, config
            ).then(response => response.data.id);
            params.id = res;
        }
        f();
        for(let i = 0; i < createdCards.lenth; i ++){
            if(!createdCards[i].classList.conteins('active')){
                createdCards.pop(createdCards[i]);
            }
        }

        if(createdCards.length > 0) {
            title.classList.add('active')
        }
        if(createdCards.length == 0){
            title.classList.remove('active')
        }
        wraperDrag.onmousedown = function(e) {

            card.style.position = 'absolute';
            document.body.appendChild(card);
            moveAt(e);

            card.style.zIndex = 2;

            function moveAt(e) {
                card.style.left = e.pageX - card.offsetWidth / 2 + 'px';
                card.style.top = e.pageY - card.offsetHeight / 2 + 'px';
            }

            document.onmousemove = function(e) {
                moveAt(e);
            };

            card.onmouseup = function() {
                document.onmousemove = null;
                card.onmouseup = null;
            };

        };

        card.ondragstart = function() {
            return false;
        };

    }
onLoad() {
       axios.get(`http://cards.danit.com.ua/cards`, config
    ).then(response => {
        const resp = response.data;
        for(let i = 0; i < resp.length; i ++) {

            const wrapperCard = document.querySelector(".wrapperCard");
            const card = document.createElement('div');
            card.setAttribute('class', 'card active');
            card.setAttribute('draggable', 'true');
            card.style.zIndex = 2;
            wrapperCard.append(card);
            const cross = document.createElement('div');
            cross.setAttribute('class', 'cross');
            cross.innerText = 'X';
            card.append(cross);
            const title = document.querySelector('.title');
            cross.addEventListener('click', function () {
                card.classList.remove('active');
                axios.delete(`http://cards.danit.com.ua/cards/${resp[i].id}`, config
                ).then(response => {console.log(response)
                });
                createdCards.pop(card);
                if(createdCards.length > 0) {
                    title.classList.add('active')
                }
                if(createdCards.length == 0){
                    title.classList.remove('active')
                }
            });
            const wraperDrag = document.createElement('div');
            wraperDrag.setAttribute('draggable', 'true');
            wraperDrag.setAttribute('class', 'wraperDrag');
            card.append(wraperDrag);
            const fio = document.createElement('div');
            fio.setAttribute('class', 'infoInCards');
            fio.setAttribute('id', 'fio');
            fio.innerHTML = resp[i]['Family Name'];
            wraperDrag.append(fio);
            const doctor = document.createElement('div');
            doctor.setAttribute('class', 'infoInCards');
            doctor.setAttribute('id', 'doctor');
            doctor.innerHTML = resp[i]['doctor'];
            wraperDrag.append(doctor);
            const addInformation = document.createElement('div');
            addInformation.setAttribute('class', 'addInformation');
            addInformation.textContent = 'Show more information';
            card.append(addInformation);

            addInformation.addEventListener('click', function () {
                card.classList.add('active-test');
                        addInformation.classList.add('active');
                const otherInf = document.createElement('div');
                otherInf.setAttribute('class', 'otherInf');
                otherInf.setAttribute('id', 'otherInf');
                card.append(otherInf);
                for (let key in resp[i]) {
                    if(key !== 'id' && key !== 'doctor'){
                        const list = document.createElement('li');
                        const ul = document.createElement('ul');
                        otherInf.append(ul);
                        ul.append(list);
                        list.innerHTML = `${key} : ${resp[i][key]}`;

                    }
                }
                const hiddenInfo = document.createElement('div');
                hiddenInfo.setAttribute('class', 'hdInf active');
                hiddenInfo.textContent = 'Hide information';
                card.append(hiddenInfo);
                hiddenInfo.addEventListener('click',function () {
                    card.classList.remove('active-test');
                    otherInf.classList.add('active');
                    hiddenInfo.textContent = '';
                    addInformation.classList.remove('active');
                })
                        });

            createdCards.push(card);

            if(createdCards.length > 0) {
                title.classList.add('active')
            }
            if(createdCards.length == 0){
                title.classList.remove('active')
            }

            wraperDrag.onmousedown = function(e) {


                card.style.position = 'absolute';
                document.body.appendChild(card);
                moveAt(e);

                card.style.zIndex = 2;

                function moveAt(e) {
                    card.style.left = e.pageX - card.offsetWidth / 2 + 'px';
                    card.style.top = e.pageY - card.offsetHeight / 2 + 'px';
                }

                document.onmousemove = function(e) {
                    moveAt(e);
                };

                card.onmouseup = function() {
                    document.onmousemove = null;
                    card.onmouseup = null;
                };

            };

            card.ondragstart = function() {
                return false;
            };



        }
    });

    }

}
let newVisit = new Visit();
newVisit.createVisit();
newVisit.selectOption();
newVisit.onLoad();
